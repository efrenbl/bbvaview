/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/paper-button/paper-button.js';

class UsuariosApp extends PolymerElement {
  static get template() {
    return html`
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      <div class="container">
        <paper-button raised class="primary" on-tap="categorizar">Agregar Usuario</paper-button>
      </div>
      <div class="card">
        <h1>Usuarios</h1>
        <div class="row">
          <br>
            <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">IdCliente</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Email</th>
                <th scope="col">Password</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">D0152313</th>
                <td>Diego</td>
                <td>Silva Aguilera</td>
                <td>diego_silva29@gmail.com</td>
                <td>******</td>
              </tr>
              <tr>
                <th scope="row">D0152363</th>
                <td>Mariana</td>
                <td>Espinoza Simon</td>
                <td>mariana_espinoza10@gmail.com</td>
                <td>******</td>
              </tr>
              <tr>
                <th scope="row">D0151626</th>
                <td>Andrea</td>
                <td>Padilla Moreno</td>
                <td>andii_padilla@gmail.com</td>
                <td>******</td>
              </tr>
              <tr>
                <th scope="row">D0153149</th>
                <td>Ricardo</td>
                <td>Contreras Perez</td>
                <td>richard_con54@gmail.com</td>
                <td>******</td>
              </tr>
              <tr>
                <th scope="row">D0153150</th>
                <td>Raul</td>
                <td>Nicolas Sanchez</td>
                <td>raul_nico34@gmail.com</td>
                <td>******</td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    `;
  }
}

window.customElements.define('usuarios-app', UsuariosApp);
