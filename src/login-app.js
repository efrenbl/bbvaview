import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-localstorage/iron-localstorage.js';
import '@polymer/iron-input/iron-input.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/app-route/app-location.js';

class LoginApp extends PolymerElement {
  static get template() {
    return html`
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="css/login.css">
      <style include="shared-styles">
        :host {
          display: block;
        }
      </style>

      <app-location route="{{route}}"></app-location>

      <iron-localstorage name="user-storage" value=""></iron-localstorage>

      <iron-ajax
        id="registerLoginAjax"
        method="post"
        content-type="application/json"
        handle-as="text"
        on-response="handleUserResponse"
        on-error="handleUserError">
      </iron-ajax>

      <div id="unauthenticated" hidden$="[[storedUser.loggedin]]">

      <!--<div id="authenticated" hidden$="[[!storedUser.loggedin]]">
        <h2>Bienvenido, [[storedUser.name]]!</h2>
        <p>You are currently logged in. You can access <a href="[[rootPath]]view3">Quotes</a>!</p>
      </div>-->

      <div class="container">
      <div class="login-box">
        <div class="login-header d-flex justify-content-center">
          <img class="img-login" src="images/logo_bbva.png"  height="25" class="align-top" alt="">
          <a href="" class="title-login"><span>[[page]]</span></a>
        </div>
        <form action="home.html">
        <div class="login-body">
          <div id="unauthenticated">
          <paper-input-container>
            <label slot="input">Username</label>
            <iron-input slot="input" bind-value="">
              <input id="username" type="text" value="" placeholder="Username">
            </iron-input>
          </paper-input-container>
          <paper-input-container>
            <label>Password</label>
            <iron-input slot="input" bind-value="">
              <input id="password" type="password" value="" placeholder="Password">
            </iron-input>
          </paper-input-container>
          <template is="dom-if" if="[[error]]">
              <p class="alert-error"><strong>Error:</strong> [[error]]</p>
          </template>
          
        </div>
        <div class="login-footer">
           <div class="wrapper-btns">
            <paper-button raised class="primary" on-tap="postLogin">Iniciar Sesión</paper-button>
            <!--<paper-button class="link" on-tap="postRegister">Sign Up</paper-button>-->
          </div>
        </div>
        </form>
      </div>
    </div>

    `;
  }
  static get properties() {
    return {
      page: {
        type: String,
        value: 'Login'
      },
      formData: {
        type: Object,
        value: {}
      },
      storedUser: Object,
      error: String
    };
  }
  
  handleUserResponse(event) {
  var response = JSON.parse(event.detail.response);

  if (response.id_token) {
    this.error = '';
    this.storedUser = {
      name: this.formData.username,
      id_token: response.id_token,
      access_token: response.access_token,
      loggedin: true
      
    };
    this.set('route.path', '/my-view3');
  }

  // reset form data
  this.formData = {};
  }

  handleUserError(event) {
    this.error = event.detail.request.xhr.response;
  }

  _setReqBody() {
    this.$.registerLoginAjax.body = this.formData;
  }

  postLogin() {
    this.$.registerLoginAjax.url = 'http://localhost:3001/sessions/create';
    this._setReqBody();
    this.$.registerLoginAjax.generateRequest();
  }
}

window.customElements.define('login-app', LoginApp);
