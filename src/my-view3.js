import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-button/paper-button.js';

class MyView3 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      <iron-ajax
        id="getQuoteAjax"
        auto
        url="http://localhost:3001/api/random-quote"
        method="get"
        handle-as="text"
        last-response="{{quote}}">
      </iron-ajax>

      <div class="card">
        <h1>Quotes</h1>
        <blockquote>[[quote]]</blockquote>
        <paper-button raised on-tap="getQuote" class="primary">Get a New Quote</paper-button>
      </div>
    `;
  }
  getQuote() {
      this.$.getQuoteAjax.generateRequest();
  }
}

window.customElements.define('my-view3', MyView3);
